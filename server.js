const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
app.use(express.json());
require('./src/routes')(app);
app.listen(3333);
module.exports = app
