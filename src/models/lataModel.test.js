const lat = require("./lataModel")

describe('latModel tests', () => {
    it('should calculate and return how many cans are needed and what size',() => {
        expect(lat.calcLiters(100)).toStrictEqual([{"size": 18, "value": 1}, {"size": 0.5, "value": 4}])
    })

})