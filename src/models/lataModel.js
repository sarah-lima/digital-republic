// calcular parede retangular

exports.calcLiters = (total)=>{
    let quantTintas = [];
    total = total/5;
    const size = [18, 3.6, 2.5, 0.5];
    for (let index = 0; index < size.length; index++) {
        const element = size[index];
        if(total>=element){
            quantTintas.push({ size: element, value: parseInt(total/element)})
            total = total%element;    
        }
    }
    return quantTintas;
}
