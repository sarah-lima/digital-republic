const calc = require('../models/lataModel')

exports.post = (req, res, next) => {
   try{
      const metros = req.body.metros;
      res.status(201).json(calc.calcLiters(metros));
   }
   catch(e){
      res.status(500).json(e);
   }
 };