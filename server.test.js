const request = require('supertest');
const lataController = require('./server');

describe('latController tests', () => {
    it('should return status and response', async () => {
        const response = await request(lataController)
        .post('/')
        .send({
            metros: 100
        });
        expect(response.status).toBe(201);
        expect(response.text).toBe('[{"size":18,"value":1},{"size":0.5,"value":4}]')
        });
})